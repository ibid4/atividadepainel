﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AtividadePainel
{
    public partial class DetalhesEndereco : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Rua"] != null)
                {
                    txtRua.Text = Session["Rua"].ToString();
                }

                if (Session["Bairro"] != null)
                {
                    txtBairro.Text = Session["Bairro"].ToString();
                }

                if (Session["Número da casa"] != null)
                {
                    txtNumero.Text = Session["Número da Casa"].ToString();
                }
                
                if (Session["Cidade"] != null)
                {
                    txtCidade.Text = Session["Cidade"].ToString();
                }

                if (Session["CEP"] != null)
                {
                    txtCEP.Text = Session["CEP"].ToString();
                }
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            Response.Redirect("InformacoesPessoais.aspx");
        }

        protected void btnProximo_Click(object sender, EventArgs e)
        {
            Session["Rua"] = txtRua.Text;
            Session["Bairro"] = txtBairro.Text;
            Session["Número da Casa"] = txtNumero.Text;
            Session["Cidade"] = txtCidade.Text;
            Session["CEP"] = txtCEP.Text;

            Response.Redirect("UsuarioSenha.aspx");
        }
    }
}