﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InformacoesPessoais.aspx.cs" Inherits="AtividadePainel.InformacoesPessoais" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Informações Pessoais</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="pnlInformacoesPessoais" runat="server" GroupingText="Informações Pessoais">
            <asp:Label runat="server" Text="Nome:" />
            <asp:TextBox ID="txtNome" runat="server" /><br />
            <asp:Label runat="server" Text="Sobrenome:" />
            <asp:TextBox ID="txtSobrenome" runat="server" /><br />
            <asp:Label runat="server" Text="Gênero:" />
            <asp:DropDownList ID="ddlGenero" runat="server">
                <asp:ListItem Text="" Value="0" />
                <asp:ListItem Text="Masculino" Value="1" />
                <asp:ListItem Text="Feminino" Value="2" />
                <asp:ListItem Text="Prefiro não informar" Value="3" />
            </asp:DropDownList><br />
            <asp:Label runat="server" Text="Celular:" />
            <asp:TextBox ID="txtCelular" runat="server" /><br />
            <asp:Button ID="btnProximo" runat="server" Text="Próximo" OnClick="btnProximo_Click" />
        </asp:Panel>
    </form>
</body>
</html>
