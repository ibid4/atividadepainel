﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AtividadePainel
{
    public partial class UsuarioSenha : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            Session.Remove("Nome");
            Session.Remove("Sobrenome");
            Session.Remove("Genero");
            Session.Remove("Telefone");
            Session.Remove("Rua");
            Session.Remove("Bairro");
            Session.Remove("Número da Casa");
            Session.Remove("Cidade");
            Session.Remove("CEP");
            Session.Remove("Telefone");
            
            lblMensagem.Text = "Seus dados foram enviados com sucesso.";
            lblMensagem.Visible = true;

            txtUsuario.Text = string.Empty;
            txtSenha.Text = string.Empty;
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            Response.Redirect("DetalhesEndereco.aspx");
        }
    }
}