﻿using System;
using System.Web.UI;

namespace AtividadePainel
{
    public partial class InformacoesPessoais : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                if (Session["Nome"] != null)
                {
                    txtNome.Text = Session["Nome"].ToString();
                }

                if (Session["Sobrenome"] != null)
                {
                    txtSobrenome.Text = Session["Sobrenome"].ToString();
                }

                if (Session["Genero"] != null)
                {
                    ddlGenero.Text = Session["Genero"].ToString();
                }
                
                if (Session["Celular"] != null)
                {
                    txtCelular.Text = Session["Celular"].ToString();
                }
            }

        }

        protected void btnProximo_Click(object sender, EventArgs e)
        {
            string nome = txtNome.Text;
            string sobrenome = txtSobrenome.Text;
            string genero = ddlGenero.SelectedValue;
            string celular = txtCelular.Text;


            Session["Nome"] = nome;
            Session["Sobrenome"] = sobrenome;
            Session["Genero"] = genero;
            Session["Celular"] = celular;

            Response.Redirect("DetalhesEndereco.aspx");
        }
    }
}