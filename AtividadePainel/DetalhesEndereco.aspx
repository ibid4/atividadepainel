﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetalhesEndereco.aspx.cs" Inherits="AtividadePainel.DetalhesEndereco" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Detalhes do Endereço</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="pnlDetalhesEndereco" runat="server" GroupingText="Detalhes do Endereço">
            <asp:Label runat="server" Text="Rua:" />
            <asp:TextBox ID="txtRua" runat="server" /><br />
            <asp:Label runat="server" Text="Bairro:" />
            <asp:TextBox ID="txtBairro" runat="server" /><br />
            <asp:Label runat="server" Text="Número da Casa:" />
            <asp:TextBox ID="txtNumero" runat="server" /><br />
            <asp:Label runat="server" Text="Cidade:" />
            <asp:TextBox ID="txtCidade" runat="server" /><br />
            <asp:Label runat="server" Text="CEP:" />
            <asp:TextBox ID="txtCEP" runat="server" /><br />
            <asp:Button ID="btnVoltar" runat="server" Text="Voltar" OnClick="btnVoltar_Click" />
            <asp:Button ID="btnProximo" runat="server" Text="Próximo" OnClick="btnProximo_Click" />
        </asp:Panel>
    </form>
</body>
</html>
