﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UsuarioSenha.aspx.cs" Inherits="AtividadePainel.UsuarioSenha" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Página Final</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="pnlCadastro" runat="server" GroupingText="Cadastro de Usuário e Senha">
            <asp:Label runat="server" Text="Usuário:" />
            <asp:TextBox ID="txtUsuario" runat="server" /><br />
            <asp:Label runat="server" Text="Senha:" />
            <asp:TextBox ID="txtSenha" runat="server" TextMode="Password" /><br />
            <asp:Button ID="btnSalvar" runat="server" Text="Salvar" OnClick="btnSalvar_Click" />
            <asp:Button ID="btnVoltar" runat="server" Text="Voltar" OnClick="btnVoltar_Click" />
            <asp:Label ID="lblMensagem" runat="server" Visible="false" />
        </asp:Panel>
    </form>
</body>
</html>